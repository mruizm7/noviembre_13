import React, {useState} from 'react';
import axios from "axios";

class Pokemon extends React.Component {
  state = {
    pokemon: undefined,
    height: window.innerHeight / 2 + "px"
  };
  componentDidMount() {
    this.getPokemon();
    window.addEventListener("resize", this.handleResize);
  }

  componentDidUpdate(prevProps) {
    if (this.props.IdPokemon !== prevProps.IdPokemon) {
      this.getPokemon();
    }
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.handleResize);
  }

  getPokemon = () => {
    axios
      .get(`https://pokeapi.co/api/v2/pokemon/${this.props.IdPokemon}/`)
      .then(res => {
        this.setState({ pokemon: res.data });
      });
  };

  handleResize = () => {
    this.setState({ height: window.innerHeight / 2 + "px" });
  };

  pokemon = () => {
    const { pokemon, height } = this.state;
    if (pokemon === undefined) return null;
    return (
      <div>
        <h1>{pokemon.name}:</h1>
        <img src={pokemon.sprites.front_default} height={height} />
      </div>
    );
  };

  render() {
    return this.pokemon();
  }
}

class RandomPokemon extends React.Component {
  state = { IdPokemon: 1 };

  changePokemon = () =>
    this.setState({ IdPokemon: Math.floor(Math.random() * 100 + 1) });

  render() {
    return (
      <div>
        <Pokemon IdPokemon={this.state.IdPokemon} />
        <button onClick={this.changePokemon}>cambiar pokemon</button>
      </div>
    );
  }
}


export default RandomPokemon;

